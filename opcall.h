/* This program is licensed under the GNU Library General Public License, version 2,
 * a copy of which is included with this program (with filename LICENSE.LGPL).
 *
 * (c) 2014 Ian Nartowicz <deadbeef@nartowicz.co.uk>
 *
 * OPEdit header.
 *
 */

#ifndef __OPCALL_H
#define __OPCALL_H

extern OggOpusFile *opcall_open(DB_FILE *fp, int *res);

#endif /* __OPCALL_H */
