/*
    DeaDBeeF - ultimate music player for GNU/Linux systems with X11
    Copyright (C) 2014 Ian Nartowicz <deadbeef@nartowicz.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <errno.h>
#include <stdbool.h>
#include <opusfile.h>
#include <deadbeef/deadbeef.h>

// #define trace(...) fprintf(stderr, __VA_ARGS__)
#define trace(...)

static int _read(void *source, unsigned char *ptr, const int bytes)
{
    trace("_read %d bytes\n", bytes);
    errno = 0;
    const size_t res = ((DB_FILE *)source)->vfs->read(ptr, 1, bytes, source);
    trace("got %d bytes, errno = %d\n", res, errno);
    return errno ? -1 : res;
}

static int _seek(void *source, const opus_int64 offset, const int whence)
{
    trace("_seek to %lld from %d\n", offset, whence);
    return ((DB_FILE *)source)->vfs->seek(source, offset, whence);
}

static opus_int64 _tell(void *source)
{
    trace("_tell\n");
    return (opus_int64)((DB_FILE *)source)->vfs->tell(source);
}

static int _close(void *source)
{
    trace("_close\n");
    ((DB_FILE *)source)->vfs->close(source);
    return 0;
}

__attribute__ ((visibility ("internal")))
OggOpusFile *opcall_open(DB_FILE *fp, int *res)
{
    const bool is_streaming = fp->vfs->is_streaming();
    trace("opcall_open%s\n", is_streaming ? " (streaming)" : "");
    const OpusFileCallbacks opcb = {
        .read = _read,
        .seek = is_streaming ? NULL : _seek,
        .tell = is_streaming ? NULL : _tell,
        .close = _close
    };
    return op_open_callbacks(fp, &opcb, NULL, 0, res);
}
