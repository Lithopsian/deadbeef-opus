PREFIX?=/usr
USE_OPUSURL?=true
OUT=opus.so
INCLUDE=-I${PREFIX}/include -I${PREFIX}/include/opus -I${PREFIX}/include/deadbeef -I/usr/local/include/opus
CC?=gcc

CFLAGS+=-fPIC -Wall -Wstrict-overflow=5 -Wno-parentheses -g -Os -std=c99 -D_GNU_SOURCE
CFLAGS+=-DHAVE_OGG_STREAM_FLUSH_FILL # ogg_stream_flush_fill() function is available (libogg v1.3.0 onwards)
CFLAGS+=-DUSE_OPUSFILE_FLOAT # decode to floats (disable for pure fixed point decoding dithered to 16-bit ints)
# CFLAGS+=-D_FILE_OFFSET_BITS=64 # for largefile support on 32 bit systems
CFLAGS+=${INCLUDE}

LDFLAGS+= -s -shared -lopusfile -lopus -logg -lm -Wl,--exclude-libs=ALL
ifeq ($(USE_OPUSURL), true)
	CFLAGS+=-DUSE_OPUSURL
	LDFLAGS+=-lopusurl -lssl -lcrypto
endif

SOURCES=opus.c opcall.c liboggedit/oggedit_opus.c liboggedit/oggedit_art.c liboggedit/oggedit_utils.c liboggedit/oggedit_internal.c

OBJECTS=$(SOURCES:.c=.o)

all: $(SOURCES) $(OUT)

$(OUT): $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) $(LDFLAGS) -o $@

opus.o: opus.c liboggedit/oggedit.h opcall.h Makefile
opcall.o: opcall.c Makefile
liboggedit/oggedit_opus.o: liboggedit/oggedit_opus.c liboggedit/oggedit_internal.h liboggedit/oggedit.h Makefile
liboggedit/oggedit_art.o: liboggedit/oggedit_art.c liboggedit/oggedit_internal.h liboggedit/oggedit.h Makefile
liboggedit/oggedit_utils.o: liboggedit/oggedit_utils.c liboggedit/oggedit_internal.h liboggedit/oggedit.h Makefile
liboggedit/oggedit_internal.o: liboggedit/oggedit_internal.c liboggedit/oggedit_internal.h liboggedit/oggedit.h Makefile

install: $(OUT)
	-mkdir -p $(PREFIX)/lib/deadbeef
	-cp $(OUT) $(PREFIX)/lib/deadbeef

uninstall:
	-rm -rf $(PREFIX)/lib/deadbeef/$(OUT)

clean:
	rm $(OBJECTS) $(OUT)
