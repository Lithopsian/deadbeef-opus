/*
  Ogg Opus plugin for DeaDBeeF

  Copyright (C) 2014-2017 Ian Nartowicz <deadbeef@nartowicz.co.uk>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <math.h>
#include <libgen.h>
#include <sys/stat.h>
#include <opus/opusfile.h>
#include <deadbeef/deadbeef.h>
#include "liboggedit/oggedit.h"
#include "opcall.h"

#define error(...) fprintf(stderr, __VA_ARGS__)
#define trace(...) fprintf(stderr, __VA_ARGS__)
//#define trace(...)
//#define microtrace(...) fprintf(stderr, __VA_ARGS__)
#define microtrace(...)
//#define readtrace(...) fprintf(stderr, __VA_ARGS__)
#define readtrace(...)

// TEMPORARY, should really be included from artwork.h
typedef void (*artwork_callback)(const char *fname, const char *artist, const char *album, void *user_data);
typedef struct {
    DB_misc_t plugin;
    char* (*get_album_art) (const char *fname, const char *artist, const char *album, int size, artwork_callback callback, void *user_data);
    void (*reset) (int fast);
    const char *(*get_default_cover) (void);
    char* (*get_album_art_sync) (const char *fname, const char *artist, const char *album, int size);
    void (*make_cache_path) (char *path, int size, const char *album, const char *artist, int img_size);
} DB_artwork_plugin_t;

#define OPUS_RATE 48000
#ifdef USE_OPUSFILE_FLOAT
    #define OPUS_IS_FLOAT 1
    #define OP_READ_FUNC op_read_float
    typedef float opus_value_t;
#else
    #define OPUS_IS_FLOAT 0
    #define OP_READ_FUNC op_read
    typedef opus_int16 opus_value_t;
#endif

enum {
    REPLAYGAIN_ALBUM_GAIN,
    REPLAYGAIN_ALBUM_PEAK,
    REPLAYGAIN_TRACK_GAIN,
    REPLAYGAIN_TRACK_PEAK,
    REPLAYGAIN_REFERENCE_LOUDNESS,
    OPUS_HEADER_GAIN,
    R128_TRACK_GAIN,
    R128_ALBUM_GAIN
};

static const char *gain_tag_names[] = {
    "REPLAYGAIN_ALBUM_GAIN",
    "REPLAYGAIN_ALBUM_PEAK",
    "REPLAYGAIN_TRACK_GAIN",
    "REPLAYGAIN_TRACK_PEAK",
    "REPLAYGAIN_REFERENCE_LOUDNESS",
    "OPUS_HEADER_GAIN",
    "R128_TRACK_GAIN",
    "R128_ALBUM_GAIN"
};

static const char *replaygain_meta_keys[] = {
    ":REPLAYGAIN_ALBUMGAIN",
    ":REPLAYGAIN_ALBUMPEAK",
    ":REPLAYGAIN_TRACKGAIN",
    ":REPLAYGAIN_TRACKPEAK"
};

#define DUMMY_REPLAYGAIN "Only used for new scan values"

#if (DB_API_VERSION_MINOR < 10)
    #define DDB_RG_SOURCE_MODE_TRACK 1
    #define DDB_RG_SOURCE_MODE_ALBUM 2
#endif

#define META_DELIMITER "\n - \n"
#define MAX_ALBUM_ART_LINE 100

typedef struct {
    DB_fileinfo_t info;
    DB_playItem_t *it;
    uint32_t hints;
    OggOpusFile *opus_file;
    int cur_link;
    uint8_t *channel_map;
} opus_info_t;

typedef struct {
    bool gain;
    bool scale;
    bool track_mode;
    bool prefer_replaygain;
    bool always_use_header_gain;
    float loudness;
    int preamp_rg;
    int preamp_norg;
} rg_data_t;

typedef struct {
    const opus_info_t *info;
    rg_data_t rg_data;
#if (DB_API_VERSION_MINOR < 10)
    bool track_initialised;
#endif
    float ema_bitrate;
} streaming_info_t;

static DB_decoder_t opus_plugin;
static DB_functions_t *deadbeef;
static streaming_info_t *streaminfo = NULL;

static uintptr_t mutex;

static int opus_start(void)
{
    mutex = deadbeef->mutex_create();
    return !mutex;
}

static int opus_stop(void)
{
    deadbeef->mutex_free(mutex);
    mutex = (uintptr_t)NULL;
    return 0;
}

static void opus_lock(void)
{
    deadbeef->mutex_lock(mutex);
}

static void opus_unlock(void)
{
    deadbeef->mutex_unlock(mutex);
}

static void report_opus_error(const int error_code)
{
    switch (error_code) {
        case OP_FALSE:
            trace("OP_FALSE - General failure\n");
            break;
        case OP_EOF:
            trace("OP_EOF - End of file\n");
            break;
        case OP_HOLE:
            trace("OP_HOLE - Missing or corrupt page sequence\n");
            break;
        case OP_EBADHEADER:
            trace("OP_EBADHEADER - Invalid header page\n");
            break;
        case OP_EBADLINK:
            trace("OP_EBADLINK - Seriously corrupt file structure\n");
            break;
        case OP_EBADPACKET:
            trace("OP_EBADPACKET - Packet cannot be decoded\n");
            break;
        case OP_EBADTIMESTAMP:
            trace("OP_EBADTIMESTAMP - Link granule position not valid\n");
            break;
        case OP_EFAULT:
            trace("OP_EFAULT - Internal data error\n");
            break;
        case OP_EIMPL:
            trace("OP_EIMPL - Feature not implemented\n");
            break;
        case OP_EINVAL:
            trace("OP_EINVAL - Invalid input parameters\n");
            break;
        case OP_ENOSEEK:
            trace("OP_ENOSEEK - Tried to seek on an unseekable stream\n");
            break;
        case OP_EREAD:
            trace("OP_EREAD - Fatal I/O error\n");
            break;
        case OP_ENOTAUDIO:
            trace("OP_ENOTAUDIO - Stream doesn't contain Opus audio\n");
            break;
        case OP_ENOTFORMAT:
            trace("OP_ENOTFORMAT - Stream doesn't contain Opus pages\n");
            break;
        case OP_EVERSION:
            trace("OP_EVERSION - Unrecognised header version\n");
            break;
        default:
            trace("libopusfile returned unknown error number\n");
            break;
    }
}

static bool is_uri(const char *uri)
{
    if (!isalpha(*uri))
        return false;

    if (!strstr(uri, "://"))
        return false;

    while (*++uri != ':')
        if (!isalnum(*uri))
            return false;

    return true;
}

static char *copy_conf_str(const char *key)
{
    const char *value = deadbeef->conf_get_str_fast(key, NULL);
    return value ? strdup(value) : NULL;
}

static float sample_time(const ogg_int64_t sample)
{
    return (float)sample / OPUS_RATE;
}

static bool units_are_db(const char *end)
{
    while (*end == ' ')
        end++;
    return tolower(*end) == 'd' && toupper(*(end+1)) == 'B' && *(end+2) == '\0';
}

static float parse_float(const char *string, const bool allow_db, const int min, const int max, const float default_value)
{
    char *end;
    const float value = strtof(string, &end);
    return *end && !(allow_db && units_are_db(end)) || value < min || value > max ? default_value : value;
}

static int parse_q78(const char *string)
{
    char *end;
    const long value = strtol(string, &end, 10);
    return *end || value < -32768 || value > 32767 ? 0 : value;
}

static int parse_opus_gain_meta(const char *value)
{
    return value && !isspace(*value) ? parse_q78(value) : 0;
}

static float parse_opus_gain_input(const char *value)
{
    const int gain = parse_q78(value);
    if (gain)
        return gain;

    return parse_float(value, true, -127, 127, 0) * 256;
}

static float parse_opus_gain_rg(const char *value)
{
    const float gain = (parse_float(value, true, -99, 99, NAN) - 5) * 256;
    return isfinite(gain) ? gain : 0;
}

static float parse_gain(const char *value)
{
    return parse_float(value, true, -99, 99, 0);
}

static float parse_peak(const char *value)
{
    return parse_float(value, false, 0, 99, 1);
}

static float parse_loudness_meta(const char *value)
{
    return value ? parse_float(value, true, 1, 107, 89) : 89;
}

static float parse_loudness_input(const char *value, const float default_value)
{
    float loudness;
    char units[5];
    const int result = sscanf(value, "%f%4s%*s", &loudness, units);
    if (result == 2) {
        if (!strcasecmp(units, "LUFS") || !strcasecmp(units, "LKFS"))
            loudness += 107;
        else if (strcasecmp(units, "dB"))
            return default_value;
    }
    else if (result == 1) {
        if (loudness > -107 && loudness <= 0)
            loudness += 107;
    }
    else {
        return default_value;
    }

    return loudness > 0 && loudness <= 107 ? loudness : default_value;
}

static bool need_bitrate(uint32_t hints)
{
#if (DB_API_VERSION_MINOR >= 8)
    return hints & DDB_DECODER_HINT_NEED_BITRATE;
#else
    return true;
#endif
}

static bool raw_signal(uint32_t hints)
{
#if (DB_API_VERSION_MINOR >= 10)
    return hints & DDB_DECODER_HINT_RAW_SIGNAL;
#else
    return !need_bitrate(hints);
#endif
}

static bool always_use_header_gain(void)
{
    return deadbeef->conf_get_int("opus.headergain", 1);
}

#if (DB_API_VERSION_MINOR >= 10)
static int rg_track_mode(void)
{
    const int source_mode = deadbeef->conf_get_int("replaygain.source_mode", DDB_RG_SOURCE_MODE_PLAYBACK_ORDER);
    if (source_mode != DDB_RG_SOURCE_MODE_PLAYBACK_ORDER)
        return source_mode == DDB_RG_SOURCE_MODE_TRACK;

    const int order = deadbeef->conf_get_int("playback.order", PLAYBACK_ORDER_LINEAR);
    return order != PLAYBACK_ORDER_SHUFFLE_ALBUMS && order != PLAYBACK_ORDER_LINEAR;
}
#endif

static void read_rg_config(rg_data_t *rg_data)
{
#if (DB_API_VERSION_MINOR >= 10)
    const int flags = deadbeef->conf_get_int("replaygain.processing_flags", 0);
    rg_data->gain = flags & DDB_RG_PROCESSING_GAIN;
    rg_data->scale = flags & DDB_RG_PROCESSING_PREVENT_CLIPPING;
    rg_data->track_mode = rg_track_mode();
    rg_data->preamp_rg = deadbeef->conf_get_int("replaygain.preamp_with_rg", 0) * 256;
    rg_data->preamp_norg = deadbeef->conf_get_int("replaygain.preamp_without_rg", 0) * 256;
#else
    const int replaygain_mode = deadbeef->conf_get_int("replaygain_mode", 0);
    rg_data->gain = replaygain_mode;
    rg_data->scale = deadbeef->conf_get_int("replaygain_scale", 0);
    rg_data->track_mode = replaygain_mode == DDB_RG_SOURCE_MODE_TRACK;
    rg_data->preamp_norg = deadbeef->conf_get_int("global_preamp", 0) * 256;
    rg_data->preamp_rg = rg_data->preamp_norg + deadbeef->conf_get_int("replaygain_preamp", 0) * 256;
#endif
    rg_data->prefer_replaygain = deadbeef->conf_get_int("opus.replaygain", 1);
    rg_data->always_use_header_gain = always_use_header_gain();
    deadbeef->conf_lock();
    rg_data->loudness = parse_loudness_input(deadbeef->conf_get_str_fast("opus.loudness", "default"), 0) * 256;
    deadbeef->conf_unlock();
}

static ogg_int64_t get_startsample(DB_playItem_t *it)
{
#if (DB_API_VERSION_MINOR >= 10)
    return deadbeef->pl_item_get_startsample(it);
#else
    return it->startsample;
#endif
}

static ogg_int64_t get_endsample(DB_playItem_t *it)
{
#if (DB_API_VERSION_MINOR >= 10)
    return deadbeef->pl_item_get_endsample(it);
#else
    return it->endsample;
#endif
}

static void set_startsample(DB_playItem_t *it, ogg_int64_t sample)
{
#if (DB_API_VERSION_MINOR >= 10)
    deadbeef->pl_item_set_startsample(it, sample);
#else
    it->startsample = sample;
#endif
}

static void set_endsample(DB_playItem_t *it, ogg_int64_t sample)
{
#if (DB_API_VERSION_MINOR >= 10)
    deadbeef->pl_item_set_endsample(it, sample);
#else
    it->endsample = sample;
#endif
}

static ogg_int64_t sample_offset(OggOpusFile *opus_file, const ogg_int64_t sample)
{
    microtrace("find byte offset for sample %lld\n", (long long)sample);
    if (sample <= 0 || sample == op_pcm_total(opus_file, -1))
        return 0;

    const int res = op_pcm_seek(opus_file, sample);
    if (res < 0) {
        report_opus_error(res);
        error("sample_offset: op_pcm_seek failed to find sample %lld: code %d\n", (long long)sample, res);
        return -1;
    }

    microtrace("offset is %lld\n", (long long)op_raw_tell(opus_file));
    return op_raw_tell(opus_file);
}

static void set_meta_int64(DB_playItem_t *it, const char *key, const int64_t value)
{
    char string[22];
    sprintf(string, "%" PRId64, value);
    deadbeef->pl_replace_meta(it, key, string);
}

static int64_t get_meta_int64(DB_playItem_t *it, const char *key, const int64_t default_value)
{
    const char *string = deadbeef->pl_find_meta_raw(it, key);
    return string ? atoll(string) : default_value;
}

static void send_event(DB_playItem_t *it, const uint32_t event_enum)
{
    ddb_event_t *event = deadbeef->event_alloc(event_enum);
    if (event) {
        deadbeef->pl_item_ref(it);
        ((ddb_event_track_t *)event)->track = it;
        deadbeef->event_send(event, 0, 0);
    }
}

static int get_track(DB_playItem_t *it, char *fname)
{
    fname[0] = '\0';
    deadbeef->pl_get_meta(it, ":URI", fname, PATH_MAX-1);
    return deadbeef->pl_find_meta_int(it, ":OGG LINK", -1);
}

static void set_base_properties(DB_playItem_t *it, const OpusHead *op)
{
    deadbeef->pl_set_meta_int(it, ":CHANNELS", op->channel_count);
    deadbeef->pl_set_meta_int(it, ":SAMPLERATE", OPUS_RATE);
}

static ogg_int64_t set_stream_properties(DB_playItem_t *it, const char *fname, OggOpusFile *opus_file, const int link, const ogg_int64_t startsample)
{
    microtrace("set_stream_properties\n");
    const ogg_int64_t samples = op_pcm_total(opus_file, link);
    ogg_int64_t endsample = startsample + samples - 1;
    set_startsample(it, startsample);
    set_endsample(it, endsample);
    microtrace("startsample=%lld, endsample=%lld (%lld samples for link %d)\n", (long long)startsample, (long long)endsample, (long long)samples, link);
    const off_t start_offset = sample_offset(opus_file, startsample-1);
    const off_t end_offset = sample_offset(opus_file, endsample);
    char *filetype = NULL;
    const off_t stream_size = oggedit_opus_stream_info(deadbeef->fopen(fname), start_offset, end_offset, &filetype);
    if (filetype) {
        deadbeef->pl_replace_meta(it, ":FILETYPE", filetype);
        free(filetype);
    }
    if (stream_size > 0) {
        set_meta_int64(it, ":STREAM SIZE", stream_size);
        const float bitrate = CHAR_BIT * stream_size / sample_time(samples) / 1000; // op_bitrate gives wrong value
        deadbeef->pl_set_meta_int(it, ":BITRATE", bitrate < INT_MAX ? lrintf(bitrate) : INT_MAX);
    }
    set_meta_int64(it, ":FILE SIZE", op_raw_total(opus_file, -1));
    return samples;
}

#ifdef USE_OPUSURL
static void server_properties_thread(void *ctx)
{
    microtrace("server_properties_thread\n");
    DB_playItem_t *it = DB_PLAYITEM(ctx);
    char url[PATH_MAX];
    get_track(it, url);
    OpusServerInfo server_info;
    deadbeef->conf_lock();
    const char *type = deadbeef->conf_get_str_fast("network.proxy.type", NULL);
    const bool proxy = deadbeef->conf_get_int("network.proxy", 0) && !strcmp("HTTP", type);
    char *proxy_address = proxy ? copy_conf_str("network.proxy.address") : NULL;
    char *proxy_user = proxy ? copy_conf_str("network.proxy.username") : NULL;
    char *proxy_password = proxy ? copy_conf_str("network.proxy.password") : NULL;
    deadbeef->conf_unlock();
    const int proxy_port = deadbeef->conf_get_int("network.proxy.port", 8080);

    OggOpusFile *opus_file = op_test_url(url, NULL,
                                         OP_HTTP_PROXY_HOST(proxy_address),
                                         OP_HTTP_PROXY_PORT(proxy_port),
                                         OP_HTTP_PROXY_USER(proxy_user),
                                         OP_HTTP_PROXY_PASS(proxy_password),
                                         OP_GET_SERVER_INFO(&server_info), NULL);
    if (opus_file) {
        trace("Received streaming server info from %s\n", url);
        deadbeef->pl_replace_meta(it, "ICY-NAME", server_info.name);
        deadbeef->pl_replace_meta(it, "ICY-DESCRIPTION", server_info.description);
        deadbeef->pl_replace_meta(it, "ICY-GENRE", server_info.genre);
        deadbeef->pl_set_meta_int(it, ":BITRATE", server_info.bitrate_kbps > INT_MAX ? INT_MAX : server_info.bitrate_kbps);
        deadbeef->pl_replace_meta(it, ":CONTENT_TYPE", server_info.content_type);
        deadbeef->pl_replace_meta(it, ":SERVER", server_info.server);
        deadbeef->pl_replace_meta(it, ":URL", server_info.url);
        opus_server_info_clear(&server_info);
        op_free(opus_file);
        deadbeef->pl_set_item_flags(it, deadbeef->pl_get_item_flags(it) | DDB_TAG_ICY);
        send_event(it, DB_EV_TRACKINFOCHANGED);
    }

    if (proxy_address)
        free(proxy_address);
    if (proxy_user)
        free(proxy_user);
    if (proxy_password)
        free(proxy_password);
    deadbeef->pl_item_unref(it);
//    deadbeef->sendmessage(DB_EV_PLAYLISTCHANGED, 0, 0, 0);
}
#endif

static void set_server_properties(DB_playItem_t *it, const int link)
{
    microtrace("set_server_properties\n");
    deadbeef->pl_set_meta_int(it, ":TRACKNUM", link);
#ifdef USE_OPUSURL
    deadbeef->pl_item_ref(it);
    deadbeef->thread_detach(deadbeef->thread_start(server_properties_thread, it));
#else
    send_event(it, DB_EV_TRACKINFOCHANGED);
//    deadbeef->sendmessage(DB_EV_PLAYLISTCHANGED, 0, 0, 0);
#endif
}

static char *cached_album_art(DB_playItem_t *it)
{
    const DB_artwork_plugin_t *plugin = (DB_artwork_plugin_t *)deadbeef->plug_get_for_id("artwork");
    if (!plugin)
        return NULL;

    char path[PATH_MAX];
    deadbeef->pl_lock();
    const char *artist = deadbeef->pl_find_meta_raw(it, "artist");
    const char *album = deadbeef->pl_find_meta_raw(it, "album");
    const char *title = album ? album : deadbeef->pl_find_meta_raw(it, "title");
    plugin->make_cache_path(path, PATH_MAX, title, artist, -1);
    deadbeef->pl_unlock();
    return strdup(path);
}

static char *format_picture_line(char *metadata_value, const char *tag_value, char *cache_image)
{
    if (metadata_value[0])
        strcat(metadata_value, "\n");

    OpusPictureTag pic;
    if (opus_picture_tag_parse(&pic, tag_value))
        return strcat(metadata_value, "Invalid album art tag");

    if (cache_image && *cache_image) {
        trace("Caching embedded image type %s to %s\n", oggedit_album_art_type(pic.type), cache_image);
        char cache_image_dir[PATH_MAX];
        mkdir(dirname(strcpy(cache_image_dir, cache_image)), 0755); // assume the parent directory already exists
        FILE *fp = fopen(cache_image, "w+b");
        if (!fp) {
            error("format_picture_line: failed to open image cache file %s for writing\n", cache_image);
        }
        else {
            *cache_image = '\0';
            fwrite(pic.data, 1, pic.data_length, fp);
            fclose(fp);
        }
    }

    char tag_line[MAX_ALBUM_ART_LINE];
    if (strlen(pic.description) > MAX_ALBUM_ART_LINE - 50)
        strcpy(pic.description+MAX_ALBUM_ART_LINE-52, "\u2026");
    snprintf(tag_line, MAX_ALBUM_ART_LINE-1, "%s%s%s %s (%" PRIu32 "x%" PRIu32 "px %0.0fkB)",
             pic.description,
             *pic.description ? " - " : "",
             oggedit_album_art_type(pic.type),
             pic.format == OP_PIC_FORMAT_PNG ? "png" : pic.format == OP_PIC_FORMAT_JPEG ? "jpeg" : "",
             pic.width,
             pic.height,
             pic.data_length/1000.f);
    opus_picture_tag_clear(&pic);
    return strcat(metadata_value, tag_line);
}

static bool is_special_tag(const char *tag) {
    return !strcasecmp(tag, gain_tag_names[REPLAYGAIN_ALBUM_GAIN]) ||
           !strcasecmp(tag, gain_tag_names[REPLAYGAIN_ALBUM_PEAK]) ||
           !strcasecmp(tag, gain_tag_names[REPLAYGAIN_TRACK_GAIN]) ||
           !strcasecmp(tag, gain_tag_names[REPLAYGAIN_TRACK_PEAK]) ||
           !strcasecmp(tag, gain_tag_names[REPLAYGAIN_REFERENCE_LOUDNESS]) ||
           !strcasecmp(tag, gain_tag_names[OPUS_HEADER_GAIN]) ||
           !strcasecmp(tag, gain_tag_names[R128_TRACK_GAIN]) ||
           !strcasecmp(tag, gain_tag_names[R128_ALBUM_GAIN]) ||
           !strcasecmp(tag, ALBUM_ART_KEY);
}

static const char *gain_meta(DB_playItem_t *it, const int tag_enum)
{
    return deadbeef->pl_find_meta_raw(it, gain_tag_names[tag_enum]);
}

static int opus_header_gain_meta(DB_playItem_t *it)
{
    return deadbeef->pl_find_meta_int(it, ":OPUS_HEADER_GAIN", 0);
}

static const char *tag_with_fallback(const bool track_mode, const char *tag, const char *alt_tag)
{
    return track_mode && tag || !alt_tag ? tag : alt_tag;
}

static opus_int32 scaled_gain_offset(DB_playItem_t *it, const uint32_t hints, const rg_data_t *rg_data)
{
    /* Shortcut when replaygain is disabled */
    if (!rg_data->gain && !rg_data->scale || raw_signal(hints)) {
        trace(rg_data->always_use_header_gain ? "Use Opus header gain (%+0.2f dB)\n" : "No gain\n", opus_header_gain_meta(it)/256.f);
        return rg_data->always_use_header_gain ? opus_header_gain_meta(it) : 0;
    }

    /* Get gain values from metadata since op_tags contents may be obsolete */
    deadbeef->pl_lock();
    const char *opus_track_gain = gain_meta(it, R128_TRACK_GAIN);
    const char *opus_album_gain = gain_meta(it, R128_ALBUM_GAIN);
    const char *rg_track_gain = gain_meta(it, REPLAYGAIN_TRACK_GAIN);
    const char *rg_album_gain = gain_meta(it, REPLAYGAIN_ALBUM_GAIN);
    const bool has_r128_tags = opus_track_gain || opus_album_gain;
    const bool use_rg_tags = (rg_track_gain || rg_album_gain) && (rg_data->prefer_replaygain || !has_r128_tags);

    /* Calculate the gain */
    float gain_offset = use_rg_tags || has_r128_tags ? rg_data->preamp_rg : rg_data->preamp_norg;
    if (rg_data->gain) {
        if (use_rg_tags) {
            gain_offset += parse_gain(tag_with_fallback(rg_data->track_mode, rg_track_gain, rg_album_gain)) * 256;
        }
        else if (has_r128_tags) {
            if (!rg_data->always_use_header_gain && opus_track_gain && !opus_album_gain)
                /* Header gain may be an album gain, so include before scaling */
                gain_offset += opus_header_gain_meta(it);
            gain_offset += parse_opus_gain_meta(rg_data->track_mode ? opus_track_gain : opus_album_gain);
        }

        /* Adjust the target loudness level */
        if (rg_data->loudness) {
            if (use_rg_tags)
                gain_offset += rg_data->loudness - parse_loudness_meta(gain_meta(it, REPLAYGAIN_REFERENCE_LOUDNESS)) * 256;
            else if (has_r128_tags)
                gain_offset += rg_data->loudness - 84 * 256;
        }
    }

    /* Scale peak volume */
    if (rg_data->scale) {
        const char *rg_track_peak = gain_meta(it, REPLAYGAIN_TRACK_PEAK);
        const char *rg_album_peak = gain_meta(it, REPLAYGAIN_ALBUM_PEAK);
        float peak = powf(10, gain_offset / 20 / 256);
        if (rg_track_peak || rg_album_peak)
            peak *= parse_peak(tag_with_fallback(rg_data->track_mode, rg_track_peak, rg_album_peak));
        if (peak > 1)
            gain_offset -= log10f(peak) * 20 * 256;
        trace("Peak %0.6f (scaling offset -%0.2f dB)\n", peak, peak > 1 ? 20 * log10f(peak) : 0);

#if (DB_API_VERSION_MINOR < 10)
        if (rg_data->preamp_norg > 0) // streamer will scale this away, so add it back
            gain_offset += rg_data->preamp_norg;
#endif
    }

    /* Add the header gain after scaling (unless already added) */
    if (rg_data->always_use_header_gain || rg_data->gain && !use_rg_tags && !(opus_track_gain && !opus_album_gain))
        gain_offset += opus_header_gain_meta(it);
    deadbeef->pl_unlock();

    trace("Gain: %+0.2f dB\n", gain_offset / 256.f);
    return lrintf(gain_offset);
}

static void apply_gain(const opus_info_t *info, rg_data_t *rg_data)
{
    /* Calculate the gain ourselves because libopusfile internal data doesn't update when the file is modified */
    microtrace("Opus: apply_gain\n");
    op_set_gain_offset(info->opus_file, OP_ABSOLUTE_GAIN, scaled_gain_offset(info->it, info->hints, rg_data));
}

static void set_opus_header_gain_meta(DB_playItem_t *it, const int value)
{
    char meta_value[20];
    sprintf(meta_value, "%d (%+0.2f dB)", value, value/256.f);
    deadbeef->pl_replace_meta(it, ":OPUS_HEADER_GAIN", meta_value);
}

static void set_gain_meta_dummy(DB_playItem_t *it, const int tag_enum)
{
    deadbeef->pl_replace_meta(it, replaygain_meta_keys[tag_enum], DUMMY_REPLAYGAIN);
}

static void set_gain_meta(DB_playItem_t *it, const OpusTags *vc, const int tag_enum)
{
    const char *key = gain_tag_names[tag_enum];
    const char *value = opus_tags_query(vc, key, 0);
    if (value)
        deadbeef->pl_add_meta(it, key, value);
}

static int copy_tag_to_meta(DB_playItem_t *it, char *tag)
{
    if (!tag)
        return -1;

    char *tag_value = strchr(tag, '=');
    if (!tag_value) {
        return -1;
    }

    *tag_value++ = '\0';
    if (is_special_tag(tag))
        return 0;

    const char *key = oggedit_map_tag(tag, "tag2meta");
    const char *old_value = deadbeef->pl_find_meta_raw(it, key);
    if (old_value) {
        /* Multiple tags with the same name are stored in metadata as a single delimited value */
        char *new_value = malloc(strlen(old_value) + strlen(META_DELIMITER) + strlen(tag_value) + 1);
        if (!new_value)
            return -1;

        sprintf(new_value, "%s" META_DELIMITER "%s", old_value, tag_value);
        deadbeef->pl_replace_meta(it, key, new_value);
        free(new_value);
    }
    else {
        deadbeef->pl_add_meta(it, key, tag_value);
    }

    return 0;
}

static DB_playItem_t *parse_tags(const OpusTags *vc, const bool strict)
{
    DB_playItem_t *it = deadbeef->pl_item_alloc();
    if (!it)
        return NULL;

    for (unsigned i = 0; i < vc->comments; i++) {
        char *user_comment = strdup(vc->user_comments[i]);
        const int error = copy_tag_to_meta(it, user_comment);
        free(user_comment);
        if (error && strict) {
            deadbeef->pl_item_unref(it);
            return NULL;
        }
    }

    set_gain_meta(it, vc, R128_ALBUM_GAIN);
    set_gain_meta(it, vc, R128_TRACK_GAIN);
    set_gain_meta(it, vc, REPLAYGAIN_ALBUM_GAIN);
    set_gain_meta(it, vc, REPLAYGAIN_ALBUM_PEAK);
    set_gain_meta(it, vc, REPLAYGAIN_TRACK_GAIN);
    set_gain_meta(it, vc, REPLAYGAIN_TRACK_PEAK);
    set_gain_meta(it, vc, REPLAYGAIN_REFERENCE_LOUDNESS);

    const int album_art_tags = opus_tags_query_count(vc, ALBUM_ART_KEY);
    if (album_art_tags > 0) {
        char *album_art_meta = calloc(album_art_tags, MAX_ALBUM_ART_LINE);
        if (!album_art_meta) {
            deadbeef->pl_item_unref(it);
            return NULL;
        }
        char *image_fname = deadbeef->conf_get_int("artwork.enable_embedded", 0) ? cached_album_art(it) : NULL;
        for (unsigned i = 0; i < album_art_tags; i++)
            format_picture_line(album_art_meta, opus_tags_query(vc, ALBUM_ART_KEY, i), image_fname);
        free(image_fname);
        deadbeef->pl_add_meta(it, ALBUM_ART_META, album_art_meta);
        free(album_art_meta);
    }

    return it;
}

static int update_metadata(DB_playItem_t *it, const OggOpusFile *opus_file, const int link)
{
    trace("update_metadata for link %d\n", link);
    const OpusTags *vc = op_tags(opus_file, link);
    if (!vc)
        return -1;

    deadbeef->pl_lock();
    DB_playItem_t *holder = parse_tags(vc, op_seekable(opus_file));
    deadbeef->pl_unlock();
    if (!holder)
        return -1;

    deadbeef->pl_delete_all_meta(it);
    deadbeef->pl_replace_meta(it, ":VENDOR", vc->vendor);
    set_opus_header_gain_meta(it, op_head(opus_file, link)->output_gain);
    set_gain_meta_dummy(it, REPLAYGAIN_ALBUM_GAIN);
    set_gain_meta_dummy(it, REPLAYGAIN_ALBUM_PEAK);
    set_gain_meta_dummy(it, REPLAYGAIN_TRACK_GAIN);
    set_gain_meta_dummy(it, REPLAYGAIN_TRACK_PEAK);

    deadbeef->pl_lock();
    DB_metaInfo_t *m = deadbeef->pl_get_metadata_head(it);
    while (m->next)
        m = m->next;
    m->next = deadbeef->pl_get_metadata_head(holder);
    deadbeef->pl_unlock();
    free(holder);

    deadbeef->pl_set_item_flags(it, deadbeef->pl_get_item_flags(it) | DDB_TAG_VORBISCOMMENTS);
    ddb_playlist_t *plt = deadbeef->plt_get_curr();
    if (plt) {
        deadbeef->plt_modified(plt);
        deadbeef->plt_unref(plt);
    }

    return 0;
}

static int seek_int(DB_fileinfo_t *_info, const ogg_int64_t sample)
{
    opus_info_t *info = (opus_info_t *)_info;
    ogg_int64_t seeksample = sample + get_startsample(info->it);
    microtrace("seek_int to %lld\n", (long long)seeksample);

    const int res = op_pcm_seek(info->opus_file, seeksample);
    if (res != 0 && res != OP_ENOSEEK) {
        report_opus_error(res);
        error("opus: op_pcm_seek returned %d\n", res);
        return -1;
    }

    _info->readpos = sample_time(sample);
    return 0;
}

static int opus_seek_sample(DB_fileinfo_t *_info, const int sample)
{
    trace("opus_seek_sample %d\n", sample);
    return seek_int(_info, sample);
}

static int opus_seek(DB_fileinfo_t *_info, const float time)
{
    trace("opus_seek: %g\n", time);
    return seek_int(_info, time * OPUS_RATE);
}

static OggOpusFile *opus_file_from_db_file(DB_FILE *fp)
{
    int res;
    OggOpusFile *opus_file = opcall_open(fp, &res);
    if (!opus_file) {
        fp->vfs->close(fp);
        report_opus_error(res);
        trace("opus_file_from_db_file: opcall_open returned %d (not Opus file?)\n", res);
    }

    return opus_file;
}

static OggOpusFile *open_opus_file(const char *fname)
{
    DB_FILE *fp = deadbeef->fopen(fname);
    if (!fp) {
        trace("open_opus_file: failed to fopen %s\n", fname);
        return NULL;
    }

    return opus_file_from_db_file(fp);
}

static DB_fileinfo_t *opus_open_int(const uint32_t hints)
{
    microtrace("opus_open_int (hints %lu)\n", (unsigned long)hints);
    opus_info_t *info = calloc(1, sizeof(opus_info_t));
    if (info) {
        info->info.plugin = &opus_plugin;
        info->info.fmt.bps = CHAR_BIT * sizeof(opus_value_t);
        info->info.fmt.is_float = OPUS_IS_FLOAT;
        info->info.fmt.samplerate = OPUS_RATE;
        info->hints = hints;
    }
    return (DB_fileinfo_t *)info;
}

static DB_fileinfo_t *opus_open(const uint32_t hints)
{
    microtrace("opus_open\n");
    return opus_open_int(hints);
}

#if (DB_API_VERSION_MINOR >= 7)
static DB_fileinfo_t *opus_open2(const uint32_t hints, DB_playItem_t *it)
{
    microtrace("opus_open2\n");
    DB_fileinfo_t *_info = opus_open_int(hints);
    if (!_info)
        return NULL;

    char fname[PATH_MAX];
    get_track(it, fname);
    _info->file = deadbeef->fopen(fname);
    if (!_info->file)
        trace("opus_open2: failed to fopen %s\n", fname);

    return _info;
}
#endif

static int opus_init(DB_fileinfo_t *_info, DB_playItem_t *it)
{
    opus_info_t *info = (opus_info_t *)_info;
    deadbeef->pl_item_ref(it);
    info->it = it;

    char fname[PATH_MAX];
    const int link = get_track(it, fname);
    trace("opus_init: track %d from %s\n", link, fname);
    if (!_info->file && !(_info->file = deadbeef->fopen(fname))) {
        trace("opus_init: failed to fopen %s\n", fname);
        return -1;
    }

    info->opus_file = opus_file_from_db_file(_info->file);
    if (!info->opus_file)
        return -1;

    const OpusHead *op = op_head(info->opus_file, link);
    info->info.fmt.channels = op->channel_count;
    _info->fmt.channelmask = (1u << (op->channel_count < 32 ? op->channel_count : 31)) - 1;
    if (op->mapping_family == 1)
        info->channel_map = oggedit_vorbis_channel_map(op->channel_count);

    if (op_seekable(info->opus_file)) {
        if (link > 0)
            info->cur_link = link;
        seek_int(_info, 0);
    }
    else {
        set_endsample(it, -1);
        set_base_properties(it, op);
        if (update_metadata(it, info->opus_file, 0))
            return -1;
        set_server_properties(it, 0);
    }

    rg_data_t rg_data;
    read_rg_config(&rg_data);
    apply_gain(info, &rg_data);
    if (need_bitrate(info->hints)) { // assume we are the one and only streamer instance
        opus_lock();
        if (!streaminfo)
            streaminfo = malloc(sizeof(streaming_info_t));
        if (streaminfo) {
            streaminfo->rg_data = rg_data;
            streaminfo->info = info;
            streaminfo->ema_bitrate = deadbeef->pl_find_meta_float(it, ":BITRATE", -1);
        }
        opus_unlock();
        deadbeef->pl_replace_meta(it, "!FILETYPE", "OggOpus");
    }

    return 0;
}

static void opus_free(DB_fileinfo_t *_info)
{
    trace("opus_free\n");
    if (!_info)
        return;

    opus_info_t *info = (opus_info_t *)_info;

    opus_lock();
    if (streaminfo && streaminfo->info == info) {
        free(streaminfo);
        streaminfo = NULL;
    }
    opus_unlock();

    if (info->it)
        deadbeef->pl_item_unref(info->it);

    if (info->opus_file)
        op_free(info->opus_file);

    if (info->channel_map)
        free(info->channel_map);

    free(_info);
}

static bool new_streaming_link_format(opus_info_t *info, const int new_link)
{
    const OpusHead *op = op_head(info->opus_file, -1);
    if (info->info.fmt.channels != op->channel_count) {
        // Streamer can't do this, so re-init the stream
        trace("Stream channel count changed from %d to %d\n", info->info.fmt.channels, op->channel_count);
        deadbeef->sendmessage(DB_EV_PAUSE, 0, 0, 0);
        deadbeef->sendmessage(DB_EV_TOGGLE_PAUSE, 0, 0, 0);
        return true;
    }

    trace("Streaming link changed from %d to %d\n", info->cur_link, new_link);
    update_metadata(info->it, info->opus_file, -1);
    set_server_properties(info->it, new_link);

    opus_lock();
    if (streaminfo && info == streaminfo->info) {
        apply_gain(info, &streaminfo->rg_data);
        send_event(info->it, DB_EV_SONGSTARTED);
    }
    opus_unlock();
    info->cur_link = new_link;
    send_event(info->it, DB_EV_TRACKINFOCHANGED);

    return false;
}

static void map_channels(opus_value_t *dest, const opus_value_t *src, const opus_value_t *end, const uint8_t *map, const int channel_count)
{
    while (src < end) {
        for (unsigned i = 0; i < channel_count; i++)
            dest[map[i]] = src[i];
        src += channel_count;
        dest += channel_count;
    }
}

static void check_for_playing(opus_info_t *info)
{
#if (DB_API_VERSION_MINOR < 10)
    microtrace("Check for playing\n");
    if (streaminfo->track_initialised)
        return;

    opus_lock();
    if (!streaminfo || streaminfo->info != info) {
        streaminfo->track_initialised = true;
        opus_unlock();
        return;
    }

    DB_playItem_t *track = deadbeef->streamer_get_playing_track();
    if (track == info->it) {
        trace("Opus track is now playing in streamer\n");
        streaminfo->track_initialised = true;
        send_event(info->it, DB_EV_TRACKINFOCHANGED);
    }
    opus_unlock();
    if (track)
        deadbeef->pl_item_unref(track);
#endif
}

static void set_streamer_bitrate(opus_info_t *info)
{
    /* Calculate an exponential moving average bitrate and send any sensible value to the streamer */
    const opus_int32 instant_bitrate = op_bitrate_instant(info->opus_file);
    if (instant_bitrate <= 0)
        return;

    float instant_kbps = instant_bitrate / 1000.f;
    if (instant_kbps > INT_MAX) // clamp on 16 bit machines
        instant_kbps = INT_MAX;
    streaminfo->ema_bitrate = streaminfo->ema_bitrate <= 0 ? instant_kbps : (streaminfo->ema_bitrate*9 + instant_kbps)/10;
    deadbeef->streamer_set_bitrate(lrintf(streaminfo->ema_bitrate));

    microtrace("Rate %0.3f kbps (from instant rate %0.3f) at %0.4fs\n", streaminfo->ema_bitrate, instant_kbps, ((DB_fileinfo_t *)info)->readpos);
}

static int opus_read(DB_fileinfo_t *_info, char *buffer, const int bytes_to_read)
{
    opus_info_t *info = (opus_info_t *)_info;

    /* Work round some streamer limitations and infobar issue #22 */
    check_for_playing(info);

    /* Don't read past the end of a known sub-track */
    int samples_to_read = bytes_to_read / sizeof(opus_value_t) / _info->fmt.channels; // all sorts of assumptions here
    if (op_link_count(info->opus_file) > 1 || deadbeef->pl_get_item_flags(info->it) & DDB_IS_SUBTRACK) {
        const ogg_int64_t samples_left = get_endsample(info->it) - op_pcm_tell(info->opus_file) + 1;
        if (samples_left < samples_to_read)
            samples_to_read = samples_left;
    }

    /* Read until we have enough bytes to satisfy streamer, or there are none left */
    opus_value_t map_buffer[info->channel_map ? samples_to_read*_info->fmt.channels : 0];
    opus_value_t *ptr = info->channel_map ? map_buffer : (opus_value_t *)buffer;
    const opus_value_t *start = ptr;
    const opus_value_t *end = ptr + samples_to_read*_info->fmt.channels;
    int samples = OP_HOLE;
    while (ptr < end && (samples > 0 || samples == OP_HOLE))
    {
        int new_link;
        samples = OP_READ_FUNC(info->opus_file, ptr, end-ptr, &new_link);

        if (samples < 0) {
            report_opus_error(samples);
            trace("opus_read: op_read returned %d\n", samples);
        }
        else if (new_link != info->cur_link && new_streaming_link_format(info, new_link)) {
            ptr = (opus_value_t *)end;
            trace("New streaming link with new output format\n");
        }
        else {
            ptr += samples * _info->fmt.channels;
            readtrace("opus_read: %d samples towards %d bytes (%d samples still required)\n", samples, bytes_to_read, end-ptr);
        }
    }

    if (info->channel_map)
        map_channels((opus_value_t *)buffer, map_buffer, ptr, info->channel_map, _info->fmt.channels);

    const uint32_t prev_readpos = _info->readpos;
    _info->readpos = sample_time(op_pcm_tell(info->opus_file) - get_startsample(info->it));
    if ((uint32_t)_info->readpos > prev_readpos && need_bitrate(info->hints))
        set_streamer_bitrate(info);

    readtrace("opus_read returning %d bytes out of %d (%d wanted)\n", (void *)ptr-(void *)start, samples_to_read*sizeof(opus_value_t), bytes_to_read);
    return (void *)ptr - (void *)start;
}

static DB_playItem_t *create_playitem(const char *fname, OggOpusFile *opus_file, const int link)
{
    DB_playItem_t *it = deadbeef->pl_item_alloc_init(fname, opus_plugin.plugin.id);
    if (!it)
        return NULL;

    if (link >= 0)
        deadbeef->pl_set_meta_int(it, ":OGG LINK", link);
    set_base_properties(it, op_head(opus_file, link));
    if (update_metadata(it, opus_file, link)) {
        deadbeef->pl_item_unref(it);
        return NULL;
    }

    return it;
}

static DB_playItem_t *insert_link(ddb_playlist_t *plt, DB_playItem_t *it, DB_playItem_t *after, const ogg_int64_t samples)
{
    DB_playItem_t *inserted = NULL;

    deadbeef->pl_lock();
    const char *cuesheet_meta = deadbeef->pl_find_meta_raw(it, "cuesheet");
    if (cuesheet_meta) {
        const char *last_sheet = strstr(cuesheet_meta, META_DELIMITER);
        const char *cuesheet = last_sheet ? last_sheet + strlen(META_DELIMITER) : cuesheet_meta;
        inserted = deadbeef->plt_insert_cue_from_buffer(plt, after, it, (uint8_t *)cuesheet, strlen(cuesheet), samples, OPUS_RATE);
        if (inserted) {
            trace("Inserted sub-tracks using embedded cuesheet\n");
            deadbeef->pl_item_unref(inserted);
        }
    }
    deadbeef->pl_unlock();

    if (!inserted) {
        deadbeef->plt_set_item_duration(NULL, it, sample_time(samples));
        inserted = deadbeef->plt_insert_item(plt, after, it);
        trace("Inserted link of %g seconds\n", sample_time(samples));
    }

    deadbeef->pl_item_unref(it);
    return inserted;
}

static DB_playItem_t *insert_chain(ddb_playlist_t *plt, DB_playItem_t *after, const char *fname, OggOpusFile *opus_file)
{
    trace("Inserting %d Opus links from Ogg chain\n", op_link_count(opus_file));
    ogg_int64_t current_sample = 0;
    for (unsigned link = 0; link < op_link_count(opus_file); link++) {
        DB_playItem_t *it = create_playitem(fname, opus_file, link);
        if (it) {
            const ogg_int64_t samples = set_stream_properties(it, fname, opus_file, link, current_sample);
            after = insert_link(plt, it, after, samples);
            current_sample += samples;
        }
    }

    return after;
}

static DB_playItem_t *insert_seekable(ddb_playlist_t *plt, DB_playItem_t *after, const char *fname, OggOpusFile *opus_file)
{
    DB_playItem_t *it = create_playitem(fname, opus_file, -1);
    if (!it)
        return NULL;

    const ogg_int64_t samples = set_stream_properties(it, fname, opus_file, -1, 0);

    DB_playItem_t *cue = deadbeef->plt_insert_cue(plt, after, it, samples, OPUS_RATE);
    if (cue) {
        trace("Inserted sub-tracks using cuesheet file\n");
        deadbeef->pl_item_unref(cue);
        deadbeef->pl_item_unref(it);
        return cue;
    }

    return insert_link(plt, it, after, samples);
}

static DB_playItem_t *insert_streaming(ddb_playlist_t *plt, DB_playItem_t *after, const char *fname, OggOpusFile *opus_file)
{
    trace("insert_streaming: unexpected to get here - %s\n", fname);
    DB_playItem_t *it = create_playitem(fname, opus_file, -1);
    if (!it)
        return NULL;

    set_endsample(it, -1);
    return insert_link(plt, it, after, -1);
}

static DB_playItem_t *opus_insert(ddb_playlist_t *plt, DB_playItem_t *after, const char *fname)
{
    trace("opus_insert file %s\n", fname);
    OggOpusFile *opus_file = open_opus_file(fname);
    if (!opus_file)
        return NULL;

    DB_playItem_t *inserted;
    if (op_link_count(opus_file) > 1)
        inserted = insert_chain(plt, after, fname, opus_file);
    else if (op_seekable(opus_file))
        inserted = insert_seekable(plt, after, fname, opus_file);
    else // streaming insert never happens?
        inserted = insert_streaming(plt, after, fname, opus_file);

    op_free(opus_file);

    return inserted;
}

static int opus_read_metadata(DB_playItem_t *it)
{
    char fname[PATH_MAX];
    const int link = get_track(it, fname);
    OggOpusFile *opus_file = open_opus_file(fname);
    if (!opus_file)
        return -1;

    trace("opus_read_metadata for file %s link %d\n", fname, link);
    if (update_metadata(it, opus_file, link)) {
        op_free(opus_file);
        return -1;
    }

    opus_lock();
    if (streaminfo && it == streaminfo->info->it)
        apply_gain(streaminfo->info, &streaminfo->rg_data);
    opus_unlock();
    op_free(opus_file);

    send_event(it, DB_EV_TRACKINFOCHANGED);
//    deadbeef->sendmessage(DB_EV_PLAYLISTCHANGED, 0, 0, 0);

    return 0;
}

static void free_opus_tags(OpusTags *tags)
{
    opus_tags_clear(tags);
    free(tags);
}

static OpusTags *add_tag(OpusTags *tags, const char *key, const char *value)
{
    if (tags) { // null after error
        const int new_count = tags->comments + 1;
        if (opus_tags_add(tags, key, value)) {
            free_opus_tags(tags);
            return NULL;
        }
        tags->comments = new_count; // libopusfile bug, fixed in 0.6
    }
    return tags;
}

static OpusTags *copy_meta_to_tags(OpusTags *tags, char *meta_key, char *meta_value)
{
    if (!meta_key || !meta_value) {
        free_opus_tags(tags);
        return NULL;
    }

    /* Process delimited metadata into multiple tags with the same name */
    const char *key = oggedit_map_tag(meta_key, "meta2tag");
    char *p;
    while (p = strstr(meta_value, META_DELIMITER)) {
        *p = '\0';
        tags = add_tag(tags, key, meta_value);
        meta_value = p + strlen(META_DELIMITER);
    }

    return add_tag(tags, key, meta_value);
}

static OpusTags *add_gain_tag(OpusTags *tags, const int tag_enum, const char *pattern, const float value)
{
    char tag_value[12];
    sprintf(tag_value, pattern, value);
    return add_tag(tags, gain_tag_names[tag_enum], tag_value);
}

static OpusTags *add_opus_gain_tag(OpusTags *tags, const int tag_enum, const float value)
{
    return add_gain_tag(tags, tag_enum, "%0.0f", value);
}

static OpusTags *add_opus_gain(OpusTags *tags, const int tag_enum, const char *value)
{
    return add_opus_gain_tag(tags, tag_enum, parse_opus_gain_input(value));
}

static OpusTags *add_opus_gain_rg(OpusTags *tags, const int tag_enum, const char *value, const char *alt_value)
{
    return add_opus_gain_tag(tags, tag_enum, parse_opus_gain_rg(value ? value : alt_value));
}

static OpusTags *add_gain_db_tag(OpusTags *tags, const int tag_enum, const char *value)
{
    return add_gain_tag(tags, tag_enum, "%0.2f dB", parse_gain(value));
}

static OpusTags *add_peak_tag(OpusTags *tags, const int tag_enum, const char *value)
{
    return add_gain_tag(tags, tag_enum, "%0.6f", parse_peak(value));
}

static OpusTags *add_loudness_tag(OpusTags *tags, const int tag_enum, const char *value)
{
    return add_gain_tag(tags, tag_enum, "%0.1f dB", parse_loudness_input(value, 89));
}

static OpusTags *merge_gain_tag(DB_playItem_t *it, const OpusTags *vc, OpusTags *tags, const int tag_enum, OpusTags *(*add_func)(OpusTags *tags, const int tag_enum, const char *value))
{
    const char *key = gain_tag_names[tag_enum];
    microtrace("merge_gain_tag %s\n", key);
    const char *meta_value = deadbeef->pl_find_meta_raw(it, key);
    if (!meta_value)
        /* Currently no metadata, don't store this tag */
        return tags;

    const char *tag_value = opus_tags_query(vc, key, 0);
    if (!tag_value || strcmp(meta_value, tag_value))
        /* Metadata is changed or new, parse and store */
        return (*add_func)(tags, tag_enum, meta_value);

    /* Tag is unchanged, store as is even if invalid */
    return add_tag(tags, key, tag_value);
}

static const char *replaygain_meta(DB_playItem_t *it, const int tag_enum)
{
    return deadbeef->pl_find_meta_raw(it, replaygain_meta_keys[tag_enum]);
}

static bool replaygain_meta_unchanged(const char *album_gain, const char *album_peak, const char *track_gain, const char *track_peak)
{
    /* Have any of the values been changed from their dummy values */
    return album_gain && !strcmp(album_gain, DUMMY_REPLAYGAIN) &&
           album_peak && !strcmp(album_peak, DUMMY_REPLAYGAIN) &&
           track_gain && !strcmp(track_gain, DUMMY_REPLAYGAIN) &&
           track_peak && !strcmp(track_peak, DUMMY_REPLAYGAIN);
}

static OpusTags *tags_list(DB_playItem_t *it, OggOpusFile *opus_file, const int link, int *output_gain)
{
    const OpusTags *vc = (OpusTags *)op_tags(opus_file, link);
    if (!vc)
        return NULL;

    /* Create gain control tags */
    OpusTags *tags = calloc(1, sizeof(OpusTags));
    const char *new_album_gain = replaygain_meta(it, REPLAYGAIN_ALBUM_GAIN);
    const char *new_album_peak = replaygain_meta(it, REPLAYGAIN_ALBUM_PEAK);
    const char *new_track_gain = replaygain_meta(it, REPLAYGAIN_TRACK_GAIN);
    const char *new_track_peak = replaygain_meta(it, REPLAYGAIN_TRACK_PEAK);
    if (replaygain_meta_unchanged(new_album_gain, new_album_peak, new_track_gain, new_track_peak)) {
        trace("opus tags_list: merge any manual replaygain metadata\n");
        const char *opus_header_gain = gain_meta(it, OPUS_HEADER_GAIN);
        if (opus_header_gain)
            *output_gain = lrintf(parse_opus_gain_input(opus_header_gain));
        tags = merge_gain_tag(it, vc, tags, R128_ALBUM_GAIN, add_opus_gain);
        tags = merge_gain_tag(it, vc, tags, R128_TRACK_GAIN, add_opus_gain);
        tags = merge_gain_tag(it, vc, tags, REPLAYGAIN_ALBUM_GAIN, add_gain_db_tag);
        tags = merge_gain_tag(it, vc, tags, REPLAYGAIN_ALBUM_PEAK, add_peak_tag);
        tags = merge_gain_tag(it, vc, tags, REPLAYGAIN_TRACK_GAIN, add_gain_db_tag);
        tags = merge_gain_tag(it, vc, tags, REPLAYGAIN_TRACK_PEAK, add_peak_tag);
        tags = merge_gain_tag(it, vc, tags, REPLAYGAIN_REFERENCE_LOUDNESS, add_loudness_tag);
    }
    else {
        trace("opus tags_list: new replaygain values, presumably from a scan\n");
        trace("%s, %s, %s, %s\n", new_album_gain, new_album_peak, new_track_gain, new_track_peak);
        if (!always_use_header_gain()) {
            trace("opus tags_list: zero out header gain being treated as replaygain\n");
            *output_gain = 0;
        }
        if (deadbeef->conf_get_int("opus.write_r128", 1) && (new_album_gain || new_track_gain)) {
            tags = add_opus_gain_rg(tags, R128_ALBUM_GAIN, new_album_gain, new_track_gain);
            tags = add_opus_gain_rg(tags, R128_TRACK_GAIN, new_track_gain, new_album_gain);
        }
        if (deadbeef->conf_get_int("opus.write_rg", 0)) {
            if (new_album_gain)
                tags = add_gain_db_tag(tags, REPLAYGAIN_ALBUM_GAIN, new_album_gain);
            if (new_album_peak)
                tags = add_peak_tag(tags, REPLAYGAIN_ALBUM_PEAK, new_album_peak);
            if (new_track_gain)
                tags = add_gain_db_tag(tags, REPLAYGAIN_TRACK_GAIN, new_track_gain);
            if (new_track_peak)
                tags = add_peak_tag(tags, REPLAYGAIN_TRACK_PEAK, new_track_peak);
        }
    }

    /* Copy non-replaygain, non-internal metadata into tags */
    for (DB_metaInfo_t *m = deadbeef->pl_get_metadata_head(it); m; m = m->next) {
        if (m->key[0] != ':' && m->key[0] != '!' && !is_special_tag(m->key)) {
            char *key = strdup(m->key);
            char *value = strdup(m->value);
            tags = copy_meta_to_tags(tags, key, value);
            free(key);
            free(value);
        }
    }

    /* Add any album art */
    if (deadbeef->pl_meta_exists(it, ALBUM_ART_META)) {
        int i = 0;
        char *tag;
        while (tag = (char *)opus_tags_query(vc, ALBUM_ART_KEY, i++))
            tags = add_tag(tags, ALBUM_ART_KEY, tag);
    }

    return tags;
}

static int opus_write_metadata(DB_playItem_t *it)
{
    char fname[PATH_MAX];
    const int link = get_track(it, fname);
    trace("opus_write_metadata for file %s link %d\n", fname, link);

    OggOpusFile *opus_file = open_opus_file(fname);
    if (!opus_file)
        return -1;

    deadbeef->pl_lock();
    const off_t stream_size = get_meta_int64(it, ":STREAM SIZE", 0);
    const off_t link_offset = sample_offset(opus_file, link > 0 ? get_startsample(it)-1 : 0);
    int output_gain = op_head(opus_file, link)->output_gain;
    OpusTags *tags = tags_list(it, opus_file, link, &output_gain);
    deadbeef->pl_unlock();
    op_free(opus_file);
    if (!tags)
        return -1;

    const off_t file_size = oggedit_write_opus_metadata(deadbeef->fopen(fname), fname, link_offset, stream_size, output_gain, tags->comments, tags->user_comments);
    free_opus_tags(tags);
    if (file_size <= OGGEDIT_EOF) {
        error("opus_write_metadata: failed to write metadata to %s: code %d\n", fname, (int)file_size);
        return -1;
    }

    set_meta_int64(it, ":FILE SIZE", file_size);
    return opus_read_metadata(it);
}

static char *process_image_tag(DB_playItem_t *it, const char *fname, const off_t link_offset, OpusTags *tags, const char *tag_value)
{
    if (!add_tag(tags, ALBUM_ART_KEY, tag_value))
        return NULL;

    deadbeef->pl_lock();
    const off_t stream_size = get_meta_int64(it, ":STREAM SIZE", 0);
    deadbeef->pl_unlock();
    const off_t file_size = oggedit_write_opus_metadata(deadbeef->fopen(fname), fname, link_offset, stream_size, INT_MIN, tags->comments, tags->user_comments);
    if (file_size <= OGGEDIT_EOF) {
        error("process_image_tag: failed to write metadata to %s: code %d\n", fname, (int)file_size);
        return NULL;
    }
    set_meta_int64(it, ":FILE SIZE", file_size);

    deadbeef->pl_lock();
    const char *test_value = deadbeef->pl_find_meta_raw(it, ALBUM_ART_META);
    const char *find_meta_value = test_value ? test_value : "";
    char *meta_value = calloc(1, strlen(find_meta_value) + MAX_ALBUM_ART_LINE);
    if (meta_value)
        format_picture_line(strcpy(meta_value, find_meta_value), tag_value, NULL);
    deadbeef->pl_unlock();
    return meta_value;
}

static char *add_album_art_tag(DB_playItem_t *it, const char *fname, const off_t link_offset, OpusTags *tags)
{
    char *image_fname = cached_album_art(it);
    trace("Embed cover art from %s to %s\n", image_fname, fname);
    DB_FILE *fp = image_fname ? deadbeef->fopen(image_fname) : NULL;
    free(image_fname);
    if (!fp) {
        trace("add_album_art_tag: cached art for %s missing or unreadable - skipping\n", fname);
        return NULL;
    }

    int res;
    char *tag_value = oggedit_album_art_tag(fp, &res);
    if (!tag_value) {
        error("add_album_art_tag: error generating cover art tag, return code %d\n", res);
        return NULL;
    }
    char *meta_value = process_image_tag(it, fname, link_offset, tags, tag_value);
    free(tag_value);
    return meta_value;
}

static OpusTags *get_tags_copy(OggOpusFile *opus_file, const int link)
{
    const OpusTags *vc = op_tags(opus_file, link);
    if (!vc)
        return NULL;

    OpusTags *tags = calloc(1, sizeof(OpusTags));
    if (!tags)
        return NULL;

    if (opus_tags_copy(tags, vc) || tags->comments != vc->comments) {
        free_opus_tags(tags);
        return NULL;
    }

    return tags;
}

static void embed_album_art(DB_playItem_t *it, va_list va)
{
    char fname[PATH_MAX];
    const int link = get_track(it, fname);

    OggOpusFile *opus_file = open_opus_file(fname);
    if (!opus_file)
        return;

    const off_t link_offset = sample_offset(opus_file, get_startsample(it)-1);
    OpusTags *tags = get_tags_copy(opus_file, link);
    op_free(opus_file);
    if (!tags)
        return;

    char *meta_value = add_album_art_tag(it, fname, link_offset, tags);
    free_opus_tags(tags);
    if (!meta_value)
        return;

    deadbeef->pl_replace_meta(it, ALBUM_ART_META, meta_value);
    free(meta_value);
    send_event(it, DB_EV_TRACKINFOCHANGED);
//    deadbeef->sendmessage(DB_EV_PLAYLISTCHANGED, 0, 0, 0);
}

static void save_file(DB_playItem_t *it, va_list va)
{
    char fname[PATH_MAX];
    get_track(it, fname);

    OggOpusFile *opus_file = open_opus_file(fname);
    if (!opus_file)
        return;
    const off_t link_offset = sample_offset(opus_file, get_startsample(it)-1);
    op_free(opus_file);

    char outname[PATH_MAX];
    int length = deadbeef->pl_format_title(it, -1, outname, PATH_MAX, -1, va_arg(va, char *));
    char outpath[PATH_MAX];
    char fpath[PATH_MAX] = "";
    if (outname[0] != '/') {
        if (is_uri(fname))
            sscanf(fname, "%*[^:]://%[^:]", fpath);
        else
            strcpy(fpath, fname);
        length = snprintf(outname, PATH_MAX-length-1, "%s/%s", dirname(fpath), strcpy(outpath, outname));
    }

    if (length > PATH_MAX-3) {
        error("save_file: output filename too long\n");
        return;
    }

    if (realpath(outname, outpath) && realpath(fname, fpath) && !strcmp(outpath, fpath)) {
        error("save_file: new filename effectively the same as source file - %s\n", fname);
        return;
    }

    trace("Saving from %s to %s\n", fname, outname);
    const int res = oggedit_write_opus_file(deadbeef->fopen(fname), outname, link_offset, (bool)va_arg(va, int));
    if (res <= OGGEDIT_EOF)
        error("save_file: failed to write file %s: code %d\n", outname, res);
}

static void for_all_selected(void (* callback)(DB_playItem_t *it, va_list va), ...)
{
    ddb_playlist_t *plt = deadbeef->plt_get_curr();
    if (!plt)
        return;

    DB_playItem_t *it = deadbeef->plt_get_first(plt, PL_MAIN);
    while (it) {
        if (deadbeef->pl_is_selected(it)) {
            va_list va;
            va_start(va, callback);
            (*callback)(it, va);
            va_end(va);
        }
        deadbeef->pl_item_unref(it);
        it = deadbeef->pl_get_next(it, PL_MAIN);
    }

    deadbeef->plt_unref(plt);
}

static int opus_embed_pics(DB_plugin_action_t *action, const int ctx)
{
    for_all_selected(embed_album_art);
    return 0;
}

static int opus_save_streams(DB_plugin_action_t *action, const int ctx)
{
    char *pattern = copy_conf_str("opus.opusname");
    for_all_selected(save_file, pattern && pattern[0] ? pattern : "%n-%t.opus", false);
    free(pattern);
    return 0;
}

static int opus_save_links(DB_plugin_action_t *action, const int ctx)
{
    char *pattern = copy_conf_str("opus.oggname");
    for_all_selected(save_file, pattern && pattern[0] ? pattern : "%n-%t.opus", true);
    free(pattern);
    return 0;
}

static void add_flag_to_all_menus(DB_plugin_action_t *menu, const unsigned flag)
{
    while (menu) {
        menu->flags |= flag;
        menu = menu->next;
    }
}

static void test_playitem(DB_playItem_t *it, va_list va)
{
    DB_plugin_action_t *menu_head = va_arg(va, DB_plugin_action_t *);

    deadbeef->pl_lock();

    const char *decoder = deadbeef->pl_find_meta_raw(it, ":DECODER");
    if (!decoder || strcmp(decoder, opus_plugin.plugin.id)) {
        add_flag_to_all_menus(menu_head, DB_ACTION_DISABLED);
    }
    else {
        add_flag_to_all_menus(menu_head, DB_ACTION_ADD_MENU);

        if (deadbeef->pl_get_item_flags(it) & DDB_IS_SUBTRACK)
            add_flag_to_all_menus(menu_head, DB_ACTION_DISABLED);

        if (is_uri(deadbeef->pl_find_meta(it, ":URI")))
            menu_head->next->next->flags |= DB_ACTION_DISABLED;

        if (!(menu_head->next->next->flags & DB_ACTION_DISABLED)) {
            char *fname = cached_album_art(it);
            struct stat stat_struct;
            if (!fname || stat(fname, &stat_struct) || !S_ISREG(stat_struct.st_mode))
                menu_head->next->next->flags |= DB_ACTION_DISABLED;
            free(fname);
        }
    }

    deadbeef->pl_unlock();
}

static DB_plugin_action_t *opus_get_actions(DB_playItem_t *it)
{
    if (!it) // Only currently show for the playitem context menu
        return NULL;

    microtrace("opus_get_actions: checking context menu\n");
    static DB_plugin_action_t save_pics_action = {
        .title = "Embed cover art",
        .name = "opus_save_pic",
        .callback2 = opus_embed_pics,
        .next = NULL
    };
    static DB_plugin_action_t save_links_action = {
        .title = "Extract Ogg link to new file",
        .name = "opus_save_link",
        .callback2 = opus_save_links,
        .next = &save_pics_action
    };
    static DB_plugin_action_t save_streams_action = {
        .title = "Demux Opus audio to new file",
        .name = "opus_save_stream",
        .callback2 = opus_save_streams,
        .next = &save_links_action
    };

    save_streams_action.flags = save_links_action.flags = save_pics_action.flags = DB_ACTION_SINGLE_TRACK | DB_ACTION_MULTIPLE_TRACKS;
    for_all_selected(test_playitem, &save_streams_action);
    if (!deadbeef->plug_get_for_id("artwork"))
        save_pics_action.flags &= ~DB_ACTION_ADD_MENU;

    return &save_streams_action;
}

static int opus_message(const uint32_t id, const uintptr_t ctx, const uint32_t p1, const uint32_t p2) {
    microtrace("opus_message type %lu\n", (unsigned long)id);
    if (id != DB_EV_CONFIGCHANGED)
        return 0;

    microtrace("opus_message: validate loudness\n");
    deadbeef->conf_lock();
    const char *conf = deadbeef->conf_get_str_fast("opus.loudness", NULL);
    if (conf && !parse_loudness_input(conf, 0)) {
        microtrace("opus_message: remove invalid (or \"default\") loudness\n");
        deadbeef->conf_remove_items("opus.loudness");
        deadbeef->sendmessage(DB_EV_CONFIGCHANGED, 0, 0, 0);
    }
    deadbeef->conf_unlock();

    opus_lock();
    if (streaminfo) {
        microtrace("opus_message: config changed and Opus track is playing\n");
        const rg_data_t *rg_data = &streaminfo->rg_data;
        rg_data_t new_rg_data;
        read_rg_config(&new_rg_data);
        if (rg_data->gain != new_rg_data.gain ||
            rg_data->scale != new_rg_data.scale ||
            rg_data->track_mode != new_rg_data.track_mode ||
            rg_data->preamp_rg != new_rg_data.preamp_rg ||
            rg_data->preamp_norg != new_rg_data.preamp_norg ||
            rg_data->prefer_replaygain != new_rg_data.prefer_replaygain ||
            rg_data->always_use_header_gain != new_rg_data.always_use_header_gain ||
            rg_data->loudness != new_rg_data.loudness) {
            trace("opus_message: replaygain config changed, updating gain\n");
            streaminfo->rg_data = new_rg_data;
            apply_gain(streaminfo->info, &streaminfo->rg_data);
        }
    }
    opus_unlock();

    return 0;
}

static DB_decoder_t opus_plugin = {
    .plugin.type = DB_PLUGIN_DECODER,
#if (DB_API_VERSION_MINOR >= 10)
    .plugin.flags = DDB_PLUGIN_FLAG_REPLAYGAIN,
#endif
    .plugin.api_vmajor = 1,
    .plugin.api_vminor = DB_API_VERSION_MINOR <= 10 ? DB_API_VERSION_MINOR : 10,
    .plugin.version_major = 1,
    .plugin.version_minor = 0,
    .plugin.id = "stdopus",
    .plugin.name = "Opus decoder",
    .plugin.website = "https://bitbucket.org/Lithopsian/deadbeef-opus/overview",
    .plugin.descr =
        "Opus decoder using the xiph.org libraries: libopusfile, libopus, and libogg,\n"
        "and the DeaDBeeF oggedit library.\n\n"
        "Can play from .opus, .ogg, or .ogv files if they contain an Opus audio stream.\n\n"
        "Supports chained and multiplexed files (only plays audio streams), Opus or\n"
        "Replaygain gain control, tag reading and writing, ICY station data, embedded\n"
        "album art, and external or embedded cuesheets.\n\n"
        "Context menu actions to:\n"
        "- extract links from Ogg chained files and save them individually;\n"
        "- extract the Opus audio stream from a multiplexed Ogg file and save it;\n"
        "- embed artwork from the cover art plugin into a tag in the file.\n\n"
    ,
    .plugin.copyright =
        "Copyright (C) 2014-2017 Ian Nartowicz <deadbeef@nartowicz.co.uk>\n\n"
        "This software is provided 'as-is', without any express or implied\n"
        "warranty.  In no event will the authors be held liable for any damages\n"
        "arising from the use of this software.\n\n"
        "Permission is granted to anyone to use this software for any purpose,\n"
        "including commercial applications, and to alter it and redistribute it\n"
        "freely, subject to the following restrictions:\n\n"
        "1. The origin of this software must not be misrepresented; you must not\n"
        "claim that you wrote the original software. If you use this software\n"
        "in a product, an acknowledgment in the product documentation would be\n"
        "appreciated but is not required.\n"
        "2. Altered source versions must be plainly marked as such, and must not be\n"
        "misrepresented as being the original software.\n"
        "3. This notice may not be removed or altered from any source distribution.\n"
    ,
    .plugin.configdialog =
        "property \"Ignore R128* tags if there are REPLAYGAIN* tags\" checkbox opus.replaygain 0;\n"
        "property \"Always use Opus header gain\" checkbox opus.headergain 1;\n"
        "property \"Reference loudness to use for playback gain\" entry opus.loudness \"default\";\n"
#if (DB_API_VERSION_MINOR >= 10)
        "property \"Write new replaygain into R128 tags\" checkbox opus.write_r128 1;\n"
        "property \"Write new replaygain into REPLAYGAIN tags\" checkbox opus.write_rg 0;\n"
#endif
        "property \"Filename to save demux'd audio stream\" entry opus.opusname \"%n-%t.opus\";\n"
        "property \"Filename to save link from chained file\" entry opus.oggname \"%n-%t.ogg\";\n"
    ,
    .plugin.start = opus_start,
    .plugin.stop = opus_stop,
    .plugin.get_actions = opus_get_actions,
    .plugin.message = opus_message,
    .open = opus_open,
#if (DB_API_VERSION_MINOR >= 7)
    .open2 = opus_open2,
#endif
    .init = opus_init,
    .free = opus_free,
    .read = opus_read,
    .seek = opus_seek,
    .seek_sample = opus_seek_sample,
    .insert = opus_insert,
    .read_metadata = opus_read_metadata,
    .write_metadata = opus_write_metadata,
    .exts = (char const *[]){"opus", "ogg", "ogv", NULL}
};

const DB_plugin_t *opus_load(DB_functions_t *api) {
    deadbeef = api;
    return DB_PLUGIN(&opus_plugin);
}
